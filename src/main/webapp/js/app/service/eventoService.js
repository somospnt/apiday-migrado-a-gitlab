myApp.factory('eventoService', ['$http', function ($http) {
        eventos = [];

        function init(url, icono) {
            eventos = [];
            return buscarEventos(url, icono);
        }

        function buscarEventos(url, icono) {
            return $http.get(url).
                    then(function (response) {
                        var data = response.data;
                        var eventoCss;
                        if (response.config.url.indexOf("feriados") > -1) {
                            eventoCss = icono + ' feriado';
                        } else {
                            eventoCss = icono + ' calendario';
                        }

                        for (i = 0; i < data.length; i++) {
                            eventos.push({
                                id:data[i].id,
                                descripcion: data[i].descripcion,
                                start: moment(data[i].fecha, 'YYYY-MM-DD').toDate(),
                                allDay: true,
                                className: eventoCss + ' apiday-evento-' + data[i].tipo.replace(' ', '')
                            });
                        }
                        return eventos;
                    });
        }

        function agregarEvento(descripcion, tipo, fecha, idCalendario, callbackOk, callbackError) {
            var fechaEventoNuevo = new Date(fecha.valueOf() + fecha.getTimezoneOffset() * 60000);
            var nuevoDia = {
                descripcion: descripcion,
                tipo: tipo,
                fecha: fechaEventoNuevo,
                idCalendario: idCalendario
            };
            $http.post("api/dias", nuevoDia).success(function () {
                callbackOk();
            }).error(function () {
                callbackError();
            });
        }
        function eliminarEvento(idEvento, callbackOk, callbackError) {
            $http.delete("api/dias/" + idEvento).success(function () {
                callbackOk();
            }).error(function () {
                callbackError();
            });
        }

        return {
            buscarEventos: buscarEventos,
            agregarEvento: agregarEvento,
            eliminarEvento: eliminarEvento,
            init: init
        };
    }]);
