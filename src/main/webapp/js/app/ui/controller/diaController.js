myApp.controller('diaController', ['$scope', '$http', '$modal', '$compile', 'eventoService', function($scope, $http, $modal, $compile, eventoService) {

        $scope.anio = moment().get('year');
        $scope.eventResource = {url: 'api/feriados/', icono: 'fa fa-calendar'};

        $scope.sumarAnio = function() {
            $scope.anio = $scope.anio + 1;
            eventoService.init($scope.eventResource.url + $scope.anio, $scope.eventResource.icono).then(dibujarEventos);
        };

        $scope.restarAnio = function() {
            $scope.anio = $scope.anio - 1;
            eventoService.init($scope.eventResource.url + $scope.anio, $scope.eventResource.icono).then(dibujarEventos);
        };

        function dibujarEventos(eventos) {
            $scope.eventSources = [];
            $scope.eventSourceAjax = {
                events: function(start, end, lalala, callback) {
                    callback(eventos);
                }};

            $scope.eventSources = [$scope.eventSourceAjax];

            $scope.onEventRender = function(event, element, view) {
                if (event.start._i.getMonth() !== moment(view.start._i).month()) {
                    $(element).addClass("apiday-otro-mes");
                    return false;
                }
                $(element).attr('tooltip', event.descripcion);
                $(element).attr('idEvento', event.id);
                $compile(element)($scope);
            },
                    $scope.uiConfig = function() {
                        var meses = [];
                        for (i = 0; i < 12; i++) {
                            var mes = {
                                height: 300,
                                editable: false,
                                defaultDate: moment({y: $scope.anio, M: i, d: 1, h: 0, m: 0, s: 0, ms: 0}).format('YYYY-MM-DD'),
                                header: {
                                    left: '',
                                    center: 'title',
                                    right: ''
                                },
                                dayClick: agregarEvento,
                                eventClick: agregarEvento,
                                eventRender: $scope.onEventRender,
                                monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                                monthNamesShort: ["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"],
                                dayNames: ["Domingo", "Lunes", "Martes", "Mi&eacute;rcoles", "Jueves", "Viernes", "S\u00e1bado"],
                                dayNamesShort: ["DOM", "LUN", "MAR", "MIE", "JUE", "VIE", "SAB"]
                            };
                            meses.push(mes);
                        }
                        return {calendarios: meses};
                    }();
        }
        $scope.$on("update_idCalendarioSeleccionado", function(event, calendario) {
            var url;
            var icono = 'fa fa-calendar';
            if (calendario) {
                icono = calendario.icono;
                url = 'api/calendarios/' + calendario.id + '/';
                $scope.idCalendarioSeleccionado = calendario.id;
            } else {
                url = 'api/feriados/';
                $scope.idCalendarioSeleccionado = null;
            }
            $scope.eventResource = {url: url, icono: icono};
            refetchCalendar();
        });

        refetchCalendar();

        function refetchCalendar() {
            eventoService.init($scope.eventResource.url + $scope.anio, $scope.eventResource.icono).then(dibujarEventos);
        }
        function agregarEvento(evento, allDay, jsEvent, view) {
            if ($scope.idCalendarioSeleccionado) {
                var randomNum = Math.random();
                var randomPar = "?t=" + randomNum;
                $modal.open({
                    resolve: {
                        fecha: function() {
                            return evento._d;
                        },
                        idCalendario: function() {
                            return $scope.idCalendarioSeleccionado;
                        },
                        evento: function() {
                            return evento;
                        },
                        callbackOk: function() {
                            return refetchCalendar;
                        }
                    },
                    templateUrl: 'js/app/ui/calendario/modalEvento.html' + randomPar,
                    controller: "eventoController"
                });
            }
        }
    }]);
