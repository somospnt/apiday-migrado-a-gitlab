myApp.controller('calendarioController', ['$scope', '$http', 'eventoService', function ($scope, $http, eventoService) {
        $scope.calendarios = [];
        var url = 'api/calendarios/';
        $http.get(url).
                success(function (data) {
                    $scope.calendarios = data;
                });
        $scope.seleccionarCalendario = function (calendario) {
            $scope.calendario = calendario;
            $scope.$broadcast('update_idCalendarioSeleccionado', $scope.calendario);
        };
        $scope.borrarCalendario = function (calendario) {
            $http.delete(url + calendario.id).success(function (data) {
                var index = $scope.calendarios.indexOf(calendario);
                $scope.calendarios.splice(index, 1);

            });
        };
        $scope.agregarCalendario = function (calendario) {
            var icono = document.getElementsByName('calendario-icono')[0].value;
            calendario.icono = 'fa '+icono;
            $http.post(url, calendario).success(function (data) {
                $scope.calendarios.push(data);
                $scope.calendario.descripcion = "";
            }).error(function () {
                alert("Ocurrio un error en el alta del calendario");
            });
        };
    }]);