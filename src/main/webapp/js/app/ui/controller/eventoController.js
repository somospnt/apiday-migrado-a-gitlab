myApp.controller('eventoController', ['$scope', 'fecha', 'idCalendario', 'evento', 'eventoService', 'callbackOk', function($scope, fecha, idCalendario, evento, eventoService, callbackOk) {
        if (evento.id) {
            $scope.tituloModal = 'Modificar evento';
        } else {
            $scope.tituloModal = 'Agregar nuevo evento';
        }
        $scope.idEvento = evento.id;
        $scope.descripcionEvento = evento.descripcion;
        $scope.accionesHabilitadas = true;
        $scope.mostrarAlta = function() {
            return $scope.accionesHabilitadas && !$scope.idEvento;
        };
        $scope.mostrarBaja = function() {
            return $scope.accionesHabilitadas && $scope.idEvento;
        };
        $scope.mostrarModificacion = function() {
            return $scope.accionesHabilitadas && $scope.idEvento;
        };

        function mostrarMensajeOk() {
            $scope.mostrarMensaje = "correcto";
            $scope.accionesHabilitadas = false;
            callbackOk();
        }
        function mostrarMensajeError() {
            $scope.mostrarMensaje = "error";
        }
        function mostrarEliminacionCorrecta() {
            $scope.mostrarMensaje = "eliminacionCorrecta";
            $scope.accionesHabilitadas = false;
            callbackOk();
        }
        $scope.agregarEvento = function(descripcionEvento) {
            eventoService.agregarEvento(descripcionEvento, "CUMPLE", fecha, idCalendario, mostrarMensajeOk, mostrarMensajeError);
        };
        $scope.eliminarEvento = function() {
            eventoService.eliminarEvento($scope.idEvento, mostrarEliminacionCorrecta, mostrarMensajeError);
        };
        $scope.modificarEvento = function(descripcionEvento) {
            eventoService.eliminarEvento($scope.idEvento, function() {
                $scope.agregarEvento(descripcionEvento);
            }, mostrarMensajeError);
        };

    }]);

