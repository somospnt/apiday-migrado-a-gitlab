<%@page pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<fmt:setLocale value="es_AR" scope="session"/>

<c:set var="req" value="${pageContext.request}" />
<c:set var="baseURL" value="${fn:replace(req.requestURL, fn:substring(req.requestURI, 1, fn:length(req.requestURI)), req.contextPath)}" />

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/x-icon" href="${baseURL}/favicon.ico"/>

        <title>{apiday}</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="css/plugins/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/sb-admin-2.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">

        <link rel="stylesheet" href="vendor/bootstrap-iconpicker/css/bootstrap-iconpicker.min.css"/>

        <!-- Custom Fonts -->
        <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        <link href="css/main.css" rel="stylesheet" type="text/css">
        <link href="css/fullcalendar.min.css" rel="stylesheet" type="text/css">



        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">
            <div ng-app="myApp">
                <div ng-controller="calendarioController">
                    <!-- Navigation -->
                    <!-- Navigation -->
                    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="calendarios.html">{apiday}</a>
                        </div>
                        <!-- /.navbar-header -->

                        <ul class="nav navbar-top-links navbar-right">
                            <!-- /.dropdown -->
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="logout"><i class="fa fa-sign-out fa-fw"></i> Salir</a>
                                    </li>
                                </ul>
                                <!-- /.dropdown-user -->
                            </li>
                            <!-- /.dropdown -->
                        </ul>

                        <!-- /.navbar-top-links -->

                        <div class="navbar-default sidebar" role="navigation">
                            <div class="sidebar-nav navbar-collapse">
                                <ul class="nav" id="side-menu" >
                                    <li class="sidebar-control">
                                        <div class="panel panel-yellow">
                                            <div class="panel-heading apiday-panel-controles">
                                                <i class="fa fa-plus-square apiday-create-calendario" ng-click="agregarCalendario(calendario)"></i>
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="Nuevo calendario..." ng-model="calendario.descripcion">
                                                    <span class="input-group-btn">
                                                        <button class="btn btn-default" data-iconset="fontawesome" data-icon="fontawesome-map-marker" data-placement="bottom" role="iconpicker"></button>
                                                    </span>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- /input-group -->
                                    </li>
                                    <li ng-repeat="calendario in calendarios">
                                        <a href="#calendario" ng-click="seleccionarCalendario(calendario)">
                                            <i class="{{calendario.icono}}"></i>
                                            {{calendario.descripcion}}
                                            <i class="fa fa-times apiday-delete-calendario" ng-click="borrarCalendario(calendario)">
                                            </i>
                                        </a>

                                        <!-- /.nav-second-level -->
                                    </li>
                                    <li class="divider apiday-divider-menu"></li>
                                    <li id="feriados">
                                        <a href="#calendario" ng-click="seleccionarCalendario()"><i class="fa fa-calendar fa-fw"></i> Feriados </i> ${calendario.descripcion}</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.sidebar-collapse -->
                        </div>
                        <!-- /.navbar-static-side -->
                    </nav>

                    <div id="page-wrapper" ng-controller="diaController">
                        <tiles:insertAttribute name="body" />
                    </div>
                </div>
            </div>
        </div>
        <!-- /#wrapper -->

        <!-- jQuery Version 1.11.0 -->
        <script src="js/jquery-1.11.0.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="js/plugins/metisMenu/metisMenu.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <!--        <script src="js/plugins/morris/raphael.min.js"></script>
                <script src="js/plugins/morris/morris.min.js"></script>
                <script src="js/plugins/morris/morris-data.js"></script>-->

        <!-- Custom Theme JavaScript -->
        <script src="js/sb-admin-2.js"></script>

        <!-- Dependencias del calendario -->
        <script src="js/vendor/angular/angular.min.js"></script>
        <script src="js/vendor/calendar/calendar.js"></script>
        <script src="js/vendor/moment/moment.js"></script>
        <script src="js/vendor/calendar/fullcalendar.min.js"></script>
        <script src="js/vendor/ui-bootstrap/ui-bootstrap-tpls-0.11.0.min.js"></script>
        <script type="text/javascript" src="vendor/bootstrap-iconpicker/js/font-awesome/fa-icon-names-4.2.0.min.js"></script>
        <script type="text/javascript" src="vendor/bootstrap-iconpicker/js/bootstrap-iconpicker.js"></script>

        <!-- Propios -->
        <script src="js/app/apiday.js"></script>
        <script src="js/app/service/eventoService.js"></script>
        <script src="js/app/service/dia/dia.js"></script>
        <script src="js/app/ui/ui.js"></script>
        <script src="js/app/ui/controller/diaController.js"></script>
        <script src="js/app/ui/controller/calendarioController.js"></script>
        <script src="js/app/ui/controller/eventoController.js"></script>

        <tiles:importAttribute name="js" scope="page"/>
        <c:if test="${not empty js}">
            <script src="${js}"></script>
        </c:if>

    </body>

</html>
