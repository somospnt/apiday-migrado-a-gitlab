<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!-- /.row -->
<div class="row apiday-console-panel">
    <div class="row" ng-model="anio">
         <div class="col-md-2 col-md-offset-5">
            <button type="button" class="btn btn-warning btn-circle" ng-click="restarAnio()"><i class="fa fa-chevron-circle-left"></i>
            </button> {{anio}}
            <button type="button" class="btn btn-warning btn-circle" ng-click="sumarAnio()"><i class="fa fa-chevron-circle-right"></i>
            </button>
        </div>
    </div>
    <div  ui-calendar="calendario" ng-repeat="calendario in uiConfig.calendarios" class="col-lg-3 calendar apiday-calendario" ng-model="eventSources" calendar="myCalendar"></div>
</div>


