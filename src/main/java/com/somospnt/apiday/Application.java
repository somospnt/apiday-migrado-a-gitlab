package com.somospnt.apiday;

import jodd.http.HttpBrowser;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@ComponentScan
@EnableAutoConfiguration
@EnableScheduling
@EnableConfigurationProperties
public class Application {

    @Bean
    public HttpBrowser crearBrowser() {
        return new HttpBrowser();
    }

}
