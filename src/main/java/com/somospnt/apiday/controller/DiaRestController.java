package com.somospnt.apiday.controller;

import com.somospnt.apiday.domain.Dia;
import com.somospnt.apiday.service.DiaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DiaRestController {

    @Autowired
    private DiaService diaService;

    @RequestMapping(value = "/api/dias", method = RequestMethod.POST)
    public Dia crear(@RequestBody Dia dia) {
        diaService.guardar(dia);
        return dia;
    }

    @RequestMapping(value = "/api/dias/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrar(@PathVariable Long id) {
        diaService.borrar(id);
    }

}
