package com.somospnt.apiday.controller;

import com.somospnt.apiday.domain.Feriado;
import com.somospnt.apiday.repository.FeriadoRepository;
import com.somospnt.apiday.service.ExtractorFeriadosService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeriadoRestController {

    @Autowired
    private ExtractorFeriadosService extractorFeriadosService;

    @Autowired
    private FeriadoRepository feriadoRepository;

    @RequestMapping(value = "/api/feriados", method = RequestMethod.POST)
    public void feridos() {
        extractorFeriadosService.obtenerFeriados();
    }

    @RequestMapping(value = "/api/feriados/{anio}", method = RequestMethod.GET)
    public List<Feriado> buscarPorAnio(@PathVariable("anio") int anio) {
        return feriadoRepository.findByAnio(anio);
    }

    @RequestMapping(value = "/api/feriados/{anio}/{mes}", method = RequestMethod.GET)
    public List<Feriado> buscarPorAnioMes(@PathVariable("anio") int anio, @PathVariable("mes") int mes) {
        return feriadoRepository.findByAnioMes(anio, mes);
    }

    @RequestMapping(value = "/api/feriados/{anio}/{mes}/{dia}", method = RequestMethod.GET)
    public Feriado buscarPorAnioMesDia(@PathVariable("anio") int anio, @PathVariable("mes") int mes, @PathVariable("dia") int dia) {
        return feriadoRepository.findByAnioMesDia(anio, mes, dia);
    }


}
