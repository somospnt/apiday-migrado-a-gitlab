package com.somospnt.apiday.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CalendarioController {


    @RequestMapping("/calendarios.html")
    public String home(Model model) {
        return "calendario";
    }

}
