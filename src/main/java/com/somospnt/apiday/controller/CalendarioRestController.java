package com.somospnt.apiday.controller;

import com.somospnt.apiday.domain.Calendario;
import com.somospnt.apiday.domain.Dia;
import com.somospnt.apiday.service.CalendarioService;
import com.somospnt.apiday.service.DiaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalendarioRestController {

    @Autowired
    private DiaService diaService;

    @Autowired
    private CalendarioService calendarioService;

    @RequestMapping(value = "/api/calendarios/{idCalendario}/{anio}", method = RequestMethod.GET)
    public List<Dia> buscarPorAnio(@PathVariable("idCalendario") Long idCalendario, @PathVariable("anio") int anio) {
        return diaService.buscarPorAnio(anio, idCalendario);
    }

    @RequestMapping(value = "/api/calendarios", method = RequestMethod.GET)
    public List<Calendario> buscarTodos() {
        return calendarioService.buscarTodos();
    }

    @RequestMapping(value = "/api/calendarios/{idCalendario}/{anio}/{mes}", method = RequestMethod.GET)
    public List<Dia> buscarPorAnioMes(@PathVariable("idCalendario") Long idCalendario, @PathVariable("anio") int anio, @PathVariable("mes") int mes) {
        return diaService.buscarPorAnioMes(anio, mes, idCalendario);
    }

    @RequestMapping(value = "/api/calendarios/{idCalendario}/{anio}/{mes}/{dia}", method = RequestMethod.GET)
    public List<Dia> buscarPorAnioMesDia(@PathVariable("idCalendario") Long idCalendario, @PathVariable("anio") int anio, @PathVariable("mes") int mes, @PathVariable("dia") int dia) {
        return diaService.buscarPorAnioMesDia(anio, mes, dia, idCalendario);
    }

    @RequestMapping(value = "/api/calendarios", method = RequestMethod.POST)
    public Calendario crear(@RequestBody Calendario calendario) {
        calendarioService.guardar(calendario);
        return calendario;
    }

    @RequestMapping(value = "/api/calendarios/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrar(@PathVariable Long id) {
        calendarioService.borrar(id);
    }

}
