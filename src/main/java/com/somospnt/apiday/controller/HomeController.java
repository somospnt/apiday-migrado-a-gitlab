package com.somospnt.apiday.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @RequestMapping("/")
    public String home() {
        return "/frontend/index.html";
    }

    @RequestMapping("/login.html")
    public String login() {
        return "login";
    }



}
