package com.somospnt.apiday.controller;

import com.somospnt.apiday.domain.Calendario;
import com.somospnt.apiday.service.UsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice(annotations = Controller.class)
public class MenuControllerAdvice {

    @Autowired
    private UsuarioService usuarioService;

    @ModelAttribute("calendarios")
    public List<Calendario> calendarios() {

        try {
            return usuarioService.buscarUsuarioLogueado().getEmpresa().getCalendarios();
        } catch (Exception e) {
            return null;
        }

    }

    @ModelAttribute("token")
    public String token() {
        try {
            return usuarioService.buscarUsuarioLogueado().getToken();
        } catch (Exception e) {
            return null;
        }

    }

}
