package com.somospnt.apiday.repository;

import com.somospnt.apiday.domain.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Long>{

    Usuario findByToken(String token);
    Usuario findByusername(String userName);

}
