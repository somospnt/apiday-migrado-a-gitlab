package com.somospnt.apiday.repository;

import com.somospnt.apiday.domain.Feriado;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface FeriadoRepository extends CrudRepository<Feriado, Long>{

    @Query(" select f from Feriado f "
         + " where YEAR(f.fecha) = ?1 "
         + " order by f.fecha asc   ")
    List<Feriado> findByAnio(int anio);

    @Query(" select f from Feriado f "
         + " where YEAR(f.fecha) = ?1 "
         + " and MONTH(f.fecha) = ?2 "
         + " order by f.fecha asc   ")
    List<Feriado> findByAnioMes(int anio, int mes);

    @Query(" select f from Feriado f "
         + " where YEAR(f.fecha) = ?1 "
         + " and MONTH(f.fecha) = ?2 "
         + " and DAY(f.fecha) = ?3"
         + " order by f.fecha asc   ")
    Feriado findByAnioMesDia(int anio, int mes, int dia);


}
