package com.somospnt.apiday.repository;

import com.somospnt.apiday.domain.Calendario;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface CalendarioRepository extends CrudRepository<Calendario, Long> {

    List<Calendario> findByIdEmpresa(Long id);

}
