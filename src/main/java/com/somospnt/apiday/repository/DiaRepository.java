package com.somospnt.apiday.repository;

import com.somospnt.apiday.domain.Dia;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface DiaRepository extends CrudRepository<Dia, Long>{

    @Query(" select d from Dia d "
         + " where YEAR(d.fecha) = ?1 "
         + " and d.idCalendario = ?2 "
         + " order by d.fecha asc   ")
    List<Dia> findByAnio(int anio, Long idCalendario);

    @Query(" select d from Dia d "
         + " where YEAR(d.fecha) = ?1 "
         + " and MONTH(d.fecha) = ?2 "
         + " and d.idCalendario = ?3 "
         + " order by d.fecha asc   ")
    List<Dia> findByAnioMes(int anio, int mes, Long idCalendario);

    @Query(" select d from Dia d "
         + " where YEAR(d.fecha) = ?1 "
         + " and MONTH(d.fecha) = ?2 "
         + " and DAY(d.fecha) = ?3 "
         + " and d.idCalendario = ?4 "
         + " order by d.fecha asc   ")
    List<Dia> findByAnioMesDia(int anio, int mes, int dia, Long idCalendario);


}
