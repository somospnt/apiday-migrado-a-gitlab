package com.somospnt.apiday.service;

import com.somospnt.apiday.domain.Calendario;
import java.util.List;

public interface CalendarioService {

    void guardar(Calendario calendario);

    void borrar(Long id);

    List<Calendario> buscarTodos();

}
