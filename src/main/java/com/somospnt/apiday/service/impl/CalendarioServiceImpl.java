package com.somospnt.apiday.service.impl;

import com.somospnt.apiday.domain.Calendario;
import com.somospnt.apiday.domain.Usuario;
import com.somospnt.apiday.repository.CalendarioRepository;
import com.somospnt.apiday.service.CalendarioService;
import com.somospnt.apiday.service.UsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CalendarioServiceImpl implements CalendarioService {

    @Autowired
    private CalendarioRepository calendarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Override
    @Transactional
    @PreAuthorize("isAuthenticated()")
    public void guardar(Calendario calendario) {
        calendario.setIdEmpresa(usuarioService.buscarUsuarioLogueado().getEmpresa().getId());
        calendarioRepository.save(calendario);
    }

    @Override
    @Transactional
    @PreAuthorize("isAuthenticated()")
    public void borrar(Long id) {
        if (esCalendarioDeUsuarioLogueado(id)) {
            calendarioRepository.delete(id);
        } else {
            throw new AccessDeniedException("Acceso denegado.");
        }
    }

    private boolean esCalendarioDeUsuarioLogueado(Long idCalendario) {
        Usuario usuario = usuarioService.buscarUsuarioLogueado();
        return usuario.getEmpresa().getCalendarios().stream().anyMatch(c -> c.getId().equals(idCalendario));
    }

    
    @Override
    @PreAuthorize("isAuthenticated()")
    public List<Calendario> buscarTodos() {
        Usuario usuario = usuarioService.buscarUsuarioLogueado();
        return calendarioRepository.findByIdEmpresa(usuario.getEmpresa().getId());
    }

}
