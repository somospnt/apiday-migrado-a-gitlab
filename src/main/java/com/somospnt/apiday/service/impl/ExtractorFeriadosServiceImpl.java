package com.somospnt.apiday.service.impl;

import com.somospnt.apiday.dao.ExtarctorFeriadosDao;
import com.somospnt.apiday.dto.FeriadoDto;
import com.somospnt.apiday.repository.FeriadoRepository;
import com.somospnt.apiday.service.ExtractorFeriadosService;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class ExtractorFeriadosServiceImpl implements ExtractorFeriadosService {

    @Autowired
    private ExtarctorFeriadosDao extarctorFeriadosDao;
    @Autowired
    private FeriadoRepository feriadoRepository;

    @Override
    @PreAuthorize("isAuthenticated()")
    @Scheduled(cron = "0 0 0 1 * ?")
    public void obtenerFeriados() {
        try {
            FeriadoDto feriadoDto = extarctorFeriadosDao.buscarferiados();
            if(feriadoDto != null) {
                feriadoRepository.deleteAll();
                feriadoDto.getFeriados().stream().forEach(p -> feriadoRepository.save(p));
            } else {
                Logger.getLogger(ExtractorFeriadosServiceImpl.class.getName()).log(Level.INFO, "La extracción de feriados no obtuvo datos");
            }
        } catch (IOException ex) {
            Logger.getLogger(ExtractorFeriadosServiceImpl.class.getName()).log(Level.SEVERE, "Ocurrio un error", ex);
        }
    }
}
