package com.somospnt.apiday.service.impl;

import com.somospnt.apiday.domain.Usuario;
import com.somospnt.apiday.repository.UsuarioRepository;
import com.somospnt.apiday.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    @PreAuthorize("isAuthenticated()")
    public Usuario buscarUsuarioLogueado() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            return usuarioRepository.findByusername(auth.getName());
        }
        return null;
    }

}
