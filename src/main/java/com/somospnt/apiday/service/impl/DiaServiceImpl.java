package com.somospnt.apiday.service.impl;

import com.somospnt.apiday.domain.Dia;
import com.somospnt.apiday.domain.Usuario;
import com.somospnt.apiday.repository.DiaRepository;
import com.somospnt.apiday.service.DiaService;
import com.somospnt.apiday.service.UsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DiaServiceImpl implements DiaService {

    @Autowired
    private DiaRepository diaRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Override
    @PreAuthorize("isAuthenticated()")
    public List<Dia> buscarPorAnio(int anio, Long idCalendario) {
        if (esCalendarioDeUsuarioLogueado(idCalendario)) {
            return diaRepository.findByAnio(anio, idCalendario);
        } else {
            throw new AccessDeniedException("Acceso denegado verifique la url");
        }
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    public List<Dia> buscarPorAnioMes(int anio, int mes, Long idCalendario) {
        if (esCalendarioDeUsuarioLogueado(idCalendario)) {
            return diaRepository.findByAnio(anio, idCalendario);
        } else {
            throw new AccessDeniedException("Acceso denegado verifique la url");
        }
    }

    @Override
    @PreAuthorize("isAuthenticated()")
    public List<Dia> buscarPorAnioMesDia(int anio, int mes, int dia, Long idCalendario) {
        if (esCalendarioDeUsuarioLogueado(idCalendario)) {
            return diaRepository.findByAnioMesDia(anio, mes, dia, idCalendario);
        } else {
            throw new AccessDeniedException("Acceso denegado verifique la url");
        }
    }

    private boolean esCalendarioDeUsuarioLogueado(Long idCalendario) {
        Usuario usuario = usuarioService.buscarUsuarioLogueado();
        return usuario.getEmpresa().getCalendarios().stream().anyMatch(c -> c.getId().equals(idCalendario));
    }

    private boolean esDiaDeCalendarioDeUsuarioLogueado(Long idDia) {
        Usuario usuario = usuarioService.buscarUsuarioLogueado();
        Dia diaAValidar = diaRepository.findOne(idDia);
        return usuario.getEmpresa().getCalendarios().stream().anyMatch(c -> c.getId().equals(diaAValidar.getIdCalendario()));
    }

    @Override
    @Transactional
    @PreAuthorize("isAuthenticated()")
    public void guardar(Dia dia) {
        if (esCalendarioDeUsuarioLogueado(dia.getIdCalendario())) {
            diaRepository.save(dia);
        } else {
            throw new AccessDeniedException("Acceso denegado.");
        }
    }

    @Override
    @Transactional
    @PreAuthorize("isAuthenticated()")
    public void borrar(Long id) {
        if (esDiaDeCalendarioDeUsuarioLogueado(id)) {
            diaRepository.delete(id);
        } else {
            throw new AccessDeniedException("Acceso denegado.");
        }
    }

}
