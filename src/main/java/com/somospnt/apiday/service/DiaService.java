package com.somospnt.apiday.service;

import com.somospnt.apiday.domain.Dia;
import java.util.List;

public interface DiaService {

    List<Dia> buscarPorAnio(int anio, Long idCalendario);

    List<Dia> buscarPorAnioMes(int anio, int mes, Long idCalendario);

    List<Dia> buscarPorAnioMesDia(int anio, int mes, int dia, Long idCalendario);

    void guardar(Dia dia);

    void borrar(Long id);

}
