package com.somospnt.apiday.service;

import com.somospnt.apiday.domain.Usuario;

public interface UsuarioService {
    
     Usuario buscarUsuarioLogueado();
    
}
