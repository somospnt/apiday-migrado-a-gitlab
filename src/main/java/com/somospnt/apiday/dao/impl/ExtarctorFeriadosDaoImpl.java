package com.somospnt.apiday.dao.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.somospnt.apiday.config.ProxyConfiguration;
import com.somospnt.apiday.dao.ExtarctorFeriadosDao;
import com.somospnt.apiday.domain.Feriado;
import com.somospnt.apiday.dto.FeriadoDto;
import com.somospnt.apiday.service.impl.ExtractorFeriadosServiceImpl;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jodd.http.HttpBrowser;
import jodd.http.HttpRequest;
import jodd.http.ProxyInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository
public class ExtarctorFeriadosDaoImpl implements ExtarctorFeriadosDao {

    @Autowired
    private ProxyConfiguration proxyConf;

    @Value("${apiday.feriados.url}")
    private String url;

    @Value("${apiday.feriados.pagina}")
    private String pagina;

    @Autowired
    private HttpBrowser httpBrowser;

    private HttpRequest request;

    @Override
    public FeriadoDto buscarferiados() throws IOException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        setProxy();
        String urlServicio = obtenerUrlServicio(url + pagina);
        String feriadosJson = null;

        if (urlServicio != null) {
            feriadosJson = obtenerFeriados(url + urlServicio);
        }

        FeriadoDto feriadosDto = null;
        if (feriadosJson != null) {
            feriadosDto = new FeriadoDto();

            ObjectMapper mapper = new ObjectMapper();
            JsonNode root = mapper.readTree(feriadosJson);
            ArrayNode items = (ArrayNode) root.get("items");

            List<Feriado> feriados = new ArrayList<>();
            for (JsonNode item : items) {
                Feriado f = new Feriado();
                f.setDescripcion(item.get("Descripcion").textValue());
                f.setTipo(item.get("Tipo").textValue());
                LocalDate date = LocalDate.parse(item.get("Fecha").textValue(), formatter);
                f.setFecha(obtenerFecha(date.getYear(), date.getMonthValue()-1, date.getDayOfMonth(), 0, 0, 0));
                feriados.add(f);
            }
            feriadosDto.setFeriados(feriados);
        }
        return feriadosDto;
    }

    private String obtenerUrlServicio(String url) {
        Logger.getLogger(ExtractorFeriadosServiceImpl.class.getName()).log(Level.INFO, "Obteniendo url para servicio de: {0}", url);
        System.out.println();
        request = HttpRequest.get(url);
        request.toString();
        httpBrowser.sendRequest(request);
        Pattern pattern = Pattern.compile("((?m)ajaxjson.*$)");
        Matcher matcher = pattern.matcher(httpBrowser.getPage());
        String urlServicio = null;
        if (matcher.find()) {
            urlServicio = matcher.group(1).replace("\";", "");
        }
        return urlServicio;
    }

    private String obtenerFeriados(String urlServicio) {
        Logger.getLogger(ExtractorFeriadosServiceImpl.class.getName()).log(Level.INFO, "Obteniendo feriados de: {0}", urlServicio);
        request = HttpRequest.get(urlServicio);
        request.toString();
        httpBrowser.sendRequest(request);
        return httpBrowser.getPage();
    }

    private void setProxy() {
        if (proxyConf.isProxy()) {
            httpBrowser.setProxyInfo(new ProxyInfo(ProxyInfo.ProxyType.HTTP, proxyConf.getHost(), proxyConf.getPort(), proxyConf.getUser(), proxyConf.getPassword()));
        }

    }

    private Date obtenerFecha(int anio, int mes, int dia, int hora, int minutos, int segundos) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(anio, mes, dia, hora, minutos, segundos);
        return calendar.getTime();
    }

}
