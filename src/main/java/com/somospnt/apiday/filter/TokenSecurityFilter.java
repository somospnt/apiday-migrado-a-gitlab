package com.somospnt.apiday.filter;

import com.somospnt.apiday.domain.Usuario;
import com.somospnt.apiday.repository.UsuarioRepository;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class TokenSecurityFilter extends OncePerRequestFilter {

    private static final Logger logger = LoggerFactory.getLogger(TokenSecurityFilter.class);

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
         @SuppressWarnings("unchecked")
        Map<String, String[]> parms = request.getParameterMap();

        if (parms.containsKey("token")) {
            String strToken = parms.get("token")[0];
            Usuario usuario = usuarioRepository.findByToken(strToken);
            if (usuario != null) {
                List<GrantedAuthority> authorities = new ArrayList<>();
                authorities.add(new SimpleGrantedAuthority("ROLE_USER"));

                UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(usuario.getUsername(), usuario.getPassword());
                token.setDetails(new WebAuthenticationDetails((HttpServletRequest) request));
                Authentication authentication = new UsernamePasswordAuthenticationToken(usuario.getUsername(), usuario.getPassword(), authorities); //this.authenticationProvider.authenticate(token);

                SecurityContextHolder.getContext().setAuthentication(authentication);
            } else {
                logger.warn("Token invalido {} desde host {}", strToken, request.getRemoteHost());
            }
        } else {
            logger.info("Token no informado");
        }
        chain.doFilter(request, response);
    }
}
