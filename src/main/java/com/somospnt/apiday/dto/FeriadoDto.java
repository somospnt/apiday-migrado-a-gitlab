package com.somospnt.apiday.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.somospnt.apiday.domain.Feriado;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FeriadoDto {

    @JsonProperty("items")
    private List<Feriado> feriados;

    public List<Feriado> getFeriados() {
        return feriados;
    }

    public void setFeriados(List<Feriado> feriados) {
        this.feriados = feriados;
    }



}
