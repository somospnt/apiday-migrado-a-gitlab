package com.somospnt.apiday.dao;

import com.somospnt.apiday.ApiDayAbstractTest;
import com.somospnt.apiday.dto.FeriadoDto;
import java.io.IOException;
import jodd.http.HttpBrowser;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class ExtarctorFeriadosDaoTest extends ApiDayAbstractTest {

    @Autowired
    private ExtarctorFeriadosDao extarctorFeriadosDao;
    
    @Autowired
    private HttpBrowser browser;
    
    @Autowired
    @Qualifier("mockRespuesta")
    private String respuesta;
    
    @Test
    public void buscarferiados_conRespuesta_retornaFeriados() throws IOException {
        when(browser.getPage()).thenReturn("ajaxjson. LALALA").thenReturn(respuesta);
        loginUsuarioAdmin();
        FeriadoDto feriados = extarctorFeriadosDao.buscarferiados();
        assertTrue(feriados.getFeriados().size() > 0);
    }
    
    @Test
    public void buscarferiados_sinRespuesta_retornaNull() throws IOException {
        when(browser.getPage()).thenReturn("ajaxjson. LALALA").thenReturn(null);
        loginUsuarioAdmin();
        FeriadoDto feriados = extarctorFeriadosDao.buscarferiados();
        assertNull(feriados);
    }

}
