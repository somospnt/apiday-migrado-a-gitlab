package com.somospnt.apiday.service;

import com.somospnt.apiday.ApiDayAbstractTest;
import com.somospnt.apiday.domain.Usuario;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;

public class UsuarioServiceTest extends ApiDayAbstractTest {

    @Autowired
    private UsuarioService usuarioService;

    @Test
    public void buscarUsuarioLogueado_conUsuariologueado_devuelveUsuario() {
        loginUsuarioAdmin();
        Usuario usuario = usuarioService.buscarUsuarioLogueado();
        assertEquals("apiday", usuario.getUsername());

    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class )
    public void buscarUsuarioLogueado_sinUsuarioLogueado_lanzaAuthenticationCredentialsNotFoundException() {
        usuarioService.buscarUsuarioLogueado();
     }

}
