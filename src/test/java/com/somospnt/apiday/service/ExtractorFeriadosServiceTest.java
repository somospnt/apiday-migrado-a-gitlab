package com.somospnt.apiday.service;

import com.somospnt.apiday.ApiDayAbstractTest;
import com.somospnt.apiday.repository.FeriadoRepository;
import java.io.IOException;
import jodd.http.HttpBrowser;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.mockito.Mockito.when;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class ExtractorFeriadosServiceTest extends ApiDayAbstractTest {

    @Autowired
    private ExtractorFeriadosService extractorFeriadosService;

    @Autowired
    private FeriadoRepository feriadoRepository;

    @Autowired
    private HttpBrowser browser;
    
    @Autowired
    @Qualifier("mockRespuesta")
    private String respuesta;

    @Test
    public void obtenerFeriados_conRespuesta_insertaFeriados() throws IOException {
        when(browser.getPage()).thenReturn("ajaxjson. LALALA").thenReturn(respuesta);
        loginUsuarioAdmin();
        long filasAntes = feriadoRepository.count();
        extractorFeriadosService.obtenerFeriados();
        long filasDespues = feriadoRepository.count();
        assertTrue(filasAntes < filasDespues);
    }
    
    @Test
    public void obtenerFeriados_sinRespuesta_noInsertaFeriados() throws IOException {
        when(browser.getPage()).thenReturn("ajaxjson. LALALA").thenReturn(null);
        loginUsuarioAdmin();
        long filasAntes = feriadoRepository.count();
        extractorFeriadosService.obtenerFeriados();
        long filasDespues = feriadoRepository.count();
        assertTrue(filasAntes == filasDespues);

    }

}
