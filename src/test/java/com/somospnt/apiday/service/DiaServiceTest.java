package com.somospnt.apiday.service;

import com.somospnt.apiday.ApiDayAbstractTest;
import com.somospnt.apiday.domain.Dia;
import com.somospnt.apiday.repository.DiaRepository;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;

public class DiaServiceTest extends ApiDayAbstractTest {

    @Autowired
    private DiaService diaService;

    @Autowired
    private DiaRepository diaRepository;

    @Test(expected = AccessDeniedException.class)
    public void buscarPorAnio_conUsuarioIncoreccto_lanzaAccessDeniedException() {
        loginUsuarioAdmin();
        diaService.buscarPorAnio(2014, 1L);
    }

    @Test
    public void buscarPorAnio_conUsuarioCoreccto_devuelveDia() {
        loginUsuarioEmpresa2();
        List<Dia> dias = diaService.buscarPorAnio(2014, 1L);
        assertFalse(dias.isEmpty());
    }

    @Test(expected = AccessDeniedException.class)
    public void buscarPorAnioMes_conUsuarioIncoreccto_lanzaAccessDeniedException() {
        loginUsuarioAdmin();
        diaService.buscarPorAnioMes(2014, LocalDate.now().getMonthValue(), 1L);
    }

    @Test
    public void buscarPorAnioMes_conUsuarioCoreccto_devuelveDia() {
        loginUsuarioEmpresa2();
        List<Dia> dias = diaService.buscarPorAnioMes(2014, 1, 1L);
        assertFalse(dias.isEmpty());
    }

    @Test(expected = AccessDeniedException.class)
    public void buscarPorAnioMesDia_conUsuarioIncoreccto_lanzaAccessDeniedException() {
        loginUsuarioAdmin();
        diaService.buscarPorAnioMesDia(2014, LocalDate.now().getMonthValue(), LocalDate.now().getDayOfMonth(), 1L);
    }

    @Test
    public void buscarPorAnioMesDia_conUsuarioCoreccto_devuelveDia() {
        loginUsuarioEmpresa2();
        List<Dia> dias = diaService.buscarPorAnioMesDia(LocalDate.now().getYear(), LocalDate.now().getMonthValue(), LocalDate.now().getDayOfMonth(), 1L);
        assertFalse(dias.isEmpty());
    }

    @Test(expected = AccessDeniedException.class)
    public void guardar_conUsuarioIncoreccto_lanzaAccessDeniedException() {
        loginUsuarioAdmin();
        Dia dia = new Dia();
        dia.setFecha(new Date());
        dia.setDescripcion("El dia");
        dia.setTipo("PP");
        dia.setIdCalendario(1L);
        diaService.guardar(dia);
    }

    @Test
    public void guardar_conUsuarioCoreccto_devuelveDia() {
        loginUsuarioEmpresa2();
        Dia dia = new Dia();
        dia.setFecha(new Date());
        dia.setDescripcion("El dia");
        dia.setTipo("PP");
        dia.setIdCalendario(1L);
        diaService.guardar(dia);
        assertTrue(dia.getId() != null);
    }

    @Test(expected = AccessDeniedException.class)
    public void borrar_conUsuarioIncoreccto_lanzaAccessDeniedException() {
        loginUsuarioAdmin();
        diaService.borrar(1L);
    }

    @Test
    public void borrar_conUsuarioCoreccto_devuelveDia() {
        long diasAntes = diaRepository.count();
        loginUsuarioEmpresa3();
        diaService.borrar(4L);
        long diasDespues = diaRepository.count();
        assertTrue(diasAntes > diasDespues);
    }

}
