package com.somospnt.apiday.service;

import com.somospnt.apiday.ApiDayAbstractTest;
import com.somospnt.apiday.domain.Calendario;
import com.somospnt.apiday.repository.CalendarioRepository;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;

public class CalendarioServiceTest extends ApiDayAbstractTest {

    @Autowired
    private CalendarioService calendarioService;

    @Autowired
    private CalendarioRepository calendarioRepository;

    @Test
    public void guardar_conUsuarioCoreccto_devuelveDia() {
        loginUsuarioEmpresa2();
        Calendario calendario = new Calendario();
        calendario.setDescripcion("pp");
        calendario.setIdEmpresa(2L);
        calendarioService.guardar(calendario);
        assertTrue(calendario.getId() != null);
    }

    @Test(expected = AccessDeniedException.class)
    public void borrar_conUsuarioIncoreccto_lanzaAccessDeniedException() {
        loginUsuarioAdmin();
        calendarioService.borrar(1L);
    }

    @Test
    public void borrar_conUsuarioCoreccto_devuelveDia() {
        long calendariosAntes = calendarioRepository.count();
        loginUsuarioEmpresa2();
        calendarioService.borrar(1L);
        long calendariosDespues = calendarioRepository.count();
        assertTrue(calendariosAntes > calendariosDespues);
    }


}
