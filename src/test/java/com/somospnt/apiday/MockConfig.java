/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.somospnt.apiday;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import jodd.http.HttpBrowser;
import static org.mockito.Mockito.mock;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

@Configuration
public class MockConfig {

    @Bean
    @Primary
    public HttpBrowser httpBrowser() {
        HttpBrowser browser = mock(HttpBrowser.class);
        return browser;
    }
    
    @Bean(name = "mockRespuesta")
    public String respuesta() throws IOException {
        Resource res = new ClassPathResource("respuesta.json");
        return new String(Files.readAllBytes(Paths.get(res.getURI())));
    }
    

}
