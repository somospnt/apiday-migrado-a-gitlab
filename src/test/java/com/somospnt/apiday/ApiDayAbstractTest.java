package com.somospnt.apiday;

import javax.sql.DataSource;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class, MockConfig.class})
@WebAppConfiguration
@ActiveProfiles("test")
@Transactional
public abstract class ApiDayAbstractTest {

    @Autowired
    private DataSource dataSource;

    protected JdbcTemplate jdbcTemplate;

    @Before
    public void prepareDatabaseConnection() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void loginUsuarioAdmin() {
        login("apiday", "adminadmin");
    }

    public void loginUsuarioEmpresa2() {
        login("empresa_2", "adminadmin");
    }
     public void loginUsuarioEmpresa3() {
        login("empresa_3", "adminadmin");
    }

    private void login(String username, String password) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
        SecurityContextHolder.getContext().setAuthentication(token);
    }

    @After
    public void logout() {
        SecurityContextHolder.clearContext();
    }

}
