DROP TABLE IF EXISTS rol;
DROP TABLE IF EXISTS usuario;
DROP TABLE IF EXISTS feriado;
DROP TABLE IF EXISTS dia;
DROP TABLE IF EXISTS calendario;
DROP TABLE IF EXISTS empresa;


CREATE TABLE empresa (
    id BIGINT identity primary key,
    nombre VARCHAR(255) NOT NULL,
    activa VARCHAR(1) NULL
);

CREATE TABLE calendario (
    id BIGINT identity primary key,
    descripcion VARCHAR(255) NOT NULL,
    id_empresa BIGINT NOT NULL,
    icono       VARCHAR(100) NULL,
    FOREIGN KEY (id_empresa) REFERENCES empresa(id)
);

CREATE TABLE dia (
    id BIGINT identity primary key,
    descripcion VARCHAR(255) NOT NULL,
    tipo VARCHAR(100) NULL,
    activo VARCHAR(1) NULL,
    fecha DATETIME NOT NULL,
    id_calendario BIGINT NULL,
    FOREIGN KEY (id_calendario) REFERENCES calendario(id)
);

CREATE TABLE feriado (
    id BIGINT identity primary key,
    descripcion VARCHAR(255) NOT NULL,
    tipo VARCHAR(1) NULL,
    activo VARCHAR(1) NULL,
    fecha DATETIME NOT NULL
);

CREATE TABLE usuario (
	id         BIGINT identity PRIMARY KEY,
	username   VARCHAR(255) NOT NULL UNIQUE,
        token VARCHAR(1000) NOT NULL,
	password   VARCHAR(255) NOT NULL,
        enabled    BOOLEAN DEFAULT TRUE,
        id_empresa BIGINT NOT NULL,
        FOREIGN KEY (id_empresa) REFERENCES empresa (id)
);

CREATE TABLE rol (
	id         BIGINT identity PRIMARY KEY,
	id_usuario BIGINT       NOT NULL,
	rol        VARCHAR(255) NOT NULL,
	FOREIGN KEY (id_usuario) REFERENCES usuario (id),
	UNIQUE (id_usuario, rol)
);




insert into empresa (id, nombre, activa)
values
    (1,'apiDay', 1),
    (2,'empresa_2', 1),
    (3,'empresa_3', 1),
    (4,'empresa_4', 1);

INSERT INTO usuario
(id, username, password, token, id_empresa)
VALUES
	(1, 'apiday', '$2a$10$iodzSOMStstDQmgofPOXGOywxiLcDHtztfNmZ0DS9h5tpb.p3vOiG', 'e1',1),
	(2, 'empresa_2', '$2a$10$iodzSOMStstDQmgofPOXGOywxiLcDHtztfNmZ0DS9h5tpb.p3vOiG','e2',2),
	(3, 'empresa_3', '$2a$10$iodzSOMStstDQmgofPOXGOywxiLcDHtztfNmZ0DS9h5tpb.p3vOiG','e3',3),
	(4, 'empresa_4', '$2a$10$iodzSOMStstDQmgofPOXGOywxiLcDHtztfNmZ0DS9h5tpb.p3vOiG','e4',4);

INSERT INTO rol
(id_usuario,    rol)
VALUES
	(1,    'ROLE_ADMIN'),
	(2,    'ROLE_USER'),
	(3,    'ROLE_USER'),
        (4,    'ROLE_USER');

INSERT INTO calendario
(id, descripcion, icono, id_empresa)
VALUES (1,'Cumples', 'fa fa-gift', 2),
       (2,'Cumples', 'fa fa-gift', 3),
       (3,'Cumples', 'fa fa-gift', 4);

INSERT INTO dia
(id, descripcion, tipo, fecha, id_calendario)
VALUES (1,'Cumple de toto', 'CUMPLE', '2014-02-01 00:00:00.00', 1),
       (2,'Cumple de tito', 'CUMPLE', '2014-03-01 00:00:00.00', 1),
       (6,'Cumple de tito', 'CUMPLE', NOW(), 1),
       (3,'Cumple de tota', 'CUMPLE', NOW(), 2),
       (4,'Cumple de tata', 'CUMPLE', NOW(), 2),
       (5,'Cumple de tita', 'CUMPLE', NOW(), 3);
