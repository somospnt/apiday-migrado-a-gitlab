CREATE TABLE empresa (
    id BIGINT AUTO_INCREMENT primary key,
    nombre VARCHAR(255) NOT NULL,
    activa VARCHAR(1) NULL
);

CREATE TABLE calendario (
    id BIGINT AUTO_INCREMENT primary key,
    descripcion VARCHAR(255) NOT NULL,
    icono       VARCHAR(100) NULL,
    id_empresa BIGINT NOT NULL,
    FOREIGN KEY (id_empresa) REFERENCES empresa(id)
);

CREATE TABLE dia (
    id BIGINT AUTO_INCREMENT primary key,
    descripcion VARCHAR(255) NOT NULL,
    tipo VARCHAR(100) NULL,
    activo VARCHAR(1) NULL,
    fecha DATETIME NOT NULL,
    id_calendario BIGINT NULL,
    FOREIGN KEY (id_calendario) REFERENCES calendario(id)
);

CREATE TABLE feriado (
    id BIGINT AUTO_INCREMENT primary key,
    descripcion VARCHAR(255) NOT NULL,
    tipo VARCHAR(1) NULL,
    activo VARCHAR(1) NULL,
    fecha DATETIME NOT NULL,
    id_empresa BIGINT NULL,
    FOREIGN KEY (id_empresa) REFERENCES empresa(id)
);

CREATE TABLE usuario (
	id         BIGINT AUTO_INCREMENT PRIMARY KEY,
	username   VARCHAR(255) NOT NULL UNIQUE,
        token VARCHAR(1000) NOT NULL,
	password   VARCHAR(255) NOT NULL,
        enabled    BOOLEAN DEFAULT TRUE,
        id_empresa BIGINT NOT NULL,
        FOREIGN KEY (id_empresa) REFERENCES empresa (id)
);

CREATE TABLE rol (
	id         BIGINT AUTO_INCREMENT PRIMARY KEY,
	id_usuario BIGINT       NOT NULL,
	rol        VARCHAR(255) NOT NULL,
	FOREIGN KEY (id_usuario) REFERENCES usuario (id),
	UNIQUE (id_usuario, rol)
);




