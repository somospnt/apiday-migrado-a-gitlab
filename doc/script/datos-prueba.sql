delete from dia;
delete from calendario;
delete from rol;
delete from usuario;
delete from empresa;

insert into empresa (id, nombre, activa)
values
    (1,'apiDay', 1),
    (2,'empresa_2', 1),
    (3,'empresa_3', 1),
    (4,'empresa_4', 1);

INSERT INTO usuario
(id, username, password, token, id_empresa)
VALUES
	(1, 'apiday', '$2a$10$iodzSOMStstDQmgofPOXGOywxiLcDHtztfNmZ0DS9h5tpb.p3vOiG', 'e1',1),
	(2, 'empresa_2', '$2a$10$iodzSOMStstDQmgofPOXGOywxiLcDHtztfNmZ0DS9h5tpb.p3vOiG','e2',2),
	(3, 'empresa_3', '$2a$10$iodzSOMStstDQmgofPOXGOywxiLcDHtztfNmZ0DS9h5tpb.p3vOiG','e3',3),
	(4, 'empresa_4', '$2a$10$iodzSOMStstDQmgofPOXGOywxiLcDHtztfNmZ0DS9h5tpb.p3vOiG','e4',4);

INSERT INTO rol
(id_usuario,    rol)
VALUES
	(1,    'ROLE_ADMIN'),
	(2,    'ROLE_USER'),
	(3,    'ROLE_USER'),
        (4,    'ROLE_USER');

INSERT INTO calendario
(id, descripcion, icono, id_empresa) 
VALUES (1,'Cumples', 'fa fa-gift', 2),
       (2,'Cumples', 'fa fa-gift', 3),
       (3,'Cumples', 'fa fa-gift', 4);

INSERT INTO dia
(id, descripcion, tipo, fecha, id_calendario)
VALUES (1,'Cumple de toto', 'CUMPLE', NOW() + INTERVAL 10 DAY, 1),
       (2,'Cumple de tito', 'CUMPLE', NOW(), 1),
       (3,'Cumple de tota', 'CUMPLE', NOW() - INTERVAL 10 DAY, 2),
       (4,'Cumple de tata', 'CUMPLE', NOW() + INTERVAL 10 DAY, 2),
       (5,'Cumple de tita', 'CUMPLE', NOW(), 3);
